# Fonction pour afficher les tâches
from ajout import tasks, add_task
from marquer_termine import mark_done
from suppression import delete_task
def display_tasks():
    if tasks:
        print("Liste des tâches :")
        for index, task in enumerate( tasks, start=1):
            print(f"{index}. {task}")
    else:
        print("Aucune tâche à afficher.")

# Boucle principale pour l'interaction avec l'utilisateur
while True:
    print("\n1. Ajouter une tâche")
    print("2. Afficher les tâches")
    print("3. Marquer une tâche comme terminée")
    print("4. Supprimer une tâche")
    print("5. Quitter")

    choice = input("Que souhaitez-vous faire ? ")

    if choice == '1':
         add_task()
    elif choice == '2':
        display_tasks()
    elif choice == '3':
        mark_done()
    elif choice == '4':
        delete_task()
    elif choice == '5':
        print("Merci d'avoir utilisé l'application de gestion de tâches.")
        break
    else:
        print("Choix invalide. Veuillez réessayer.")










 
