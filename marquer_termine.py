# Fonction pour marquer une tâche comme terminée
from ajout import tasks

def mark_done():
    if tasks:
        #display_tasks()
        choice = input("Entrez le numéro de la tâche à marquer comme terminée : ")
        try:
            choice = int(choice)
            if 1 <= choice <= len(tasks):
                task_done = tasks.pop(choice - 1)
                print(f"Tâche '{task_done}' marquée comme terminée avec succès !")
            else:
                print("Choix invalide. Veuillez entrer un numéro valide.")
        except ValueError:
            print("Choix invalide. Veuillez entrer un numéro valide.")
    else:
        print("Il n'y a pas de tâches à marquer comme terminées.")

