# Liste de taches 
## 1- Implementation de la logique permettant l'ajout d'une tache.
![image 1](Capture 1/code 1.png)
## 2- Implementation de la logique permettant de marquer une tache comme termin�e.
![image 2](capture 2/code 2.png)
## 3- Implementation de la logique permettant de supprimer une tache.
![image 3](capture3/code3.png)
## 4- Implementation de la logique permettant d'afficher les taches 
![image 4](Capture4/code4.png)
![image 4](Capture4/code4suite.png)