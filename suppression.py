# Fonction pour supprimer une tâche
from ajout import tasks
def delete_task():
    if tasks:
        choice = input("Entrez le numéro de la tâche à supprimer : ")
        try:
            choice = int(choice)
            if 1 <= choice <= len(tasks):
                deleted_task = tasks.pop(choice - 1)
                print(f"Tâche '{deleted_task}' supprimée avec succès !")
            else:
                print("Choix invalide. Veuillez entrer un numéro valide.")
        except ValueError:
            print("Choix invalide. Veuillez entrer un numéro valide.")
    else:
        print("Il n'y a pas de tâches à supprimer.") 
